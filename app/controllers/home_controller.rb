class HomeController < ApplicationController
before_filter :authenticate_user!, :except => :index

	def index
		 @title = 'Home'
	end

	def leaderboard
		 @title = 'Leaderboard'
	end
end
