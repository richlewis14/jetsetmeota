class JetSet
require 'httparty'

AUTHPATH="http://ota-jetsetme.herokuapp.com/api/authentication/request_token?"
CONFIRMPATH="http://ota-jetsetme.herokuapp.com/api/authentication/confirm_token?"
DEVID=ENV["DEVID"]
APPID=ENV["APPID"]
HEADERS = {"X-DEVID" => DEVID, "X-APPID" => APPID, "Content-type" => "application/json"}


  def self.get_auth_token(msisdn)
  	msisdn = "msisdn=" + msisdn
  	response = HTTParty.post(AUTHPATH + msisdn, :headers => HEADERS)
  	token = JSON.parse(response)["token"]
  end

  def self.get_auth_key(msisdn, pin, token)
  	msisdn = "msisdn=" + msisdn
  	pin = "&pin=" + pin
  	token = "&token=" + token
  	response = HTTParty.post(CONFIRMPATH + msisdn + pin + token, :headers => HEADERS)
  	key = JSON.parse(response)["auth_key"]
  end

end